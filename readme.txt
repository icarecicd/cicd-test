
==========================================================================================================================================================================================
$ curl --request PUT -k -v --upload-file "readme.txt" -u jenkins_user:Password001 https://nexus.icare.nsw.gov.au/repository/bcpc-release-repo/pcbc_repo/readme/1.0/readme-1.0-debug.txt
==========================================================================================================================================================================================
11e1f67443d5c42e79b41bc0e9fd019ea3 : auto

11ab9816f3f143ef663c261861b3f4e19a : admin

===========================================================================================================================================================
keytool -import -trustcacerts -keystore cacerts -storepass changeit -noprompt -alias nexusCert -file C:\nexusCert.cer

/sample_repo/sample/2.0/readme.md

 -DgroupId=pcbc_repo -DartifactId=myname -Dversion=1.0  

curl -k -v -u jenkins_user:Password001 --upload-file README.md https://nexus.icare.nsw.gov.au/repository/bcpc-release-repo/com/example/artifact/1.0.0/readme.md

curl -v -F r=pcbc_repo -F e=jar -F g=yourgroup -F a=myname -F v=1.1 -F c=yourclassifier -F p=jar -F file=readme.txt
-u jenkins:Password001 https://nexus.icare.nsw.gov.au/repository/bcpc-release-repo

curl -v -u admin:admin123 --upload-file myArtifact.jar http://nexusURL:nexusPORT/repository/myRepository/com/my/group/myArtifact/1.0.0-RC1/myArtifact-1.0.0-RC1.jar


curl -k -v -u jenkins_user:Password001 --upload-file readme.zip https://nexus.icare.nsw.gov.au/repository/bcpc-release-repo/com/example/artifact/1.0.0/readme-1.0.0.zip


mvn deploy:deploy-file \
    -Durl=https://nexus.icare.nsw.gov.au/repository/ \
    -DrepositoryId=$REPO_ID \
    -DgroupId=org.myorg \
    -DartifactId=myproj \
    -Dversion=1.2.3  \
    -Dpackaging=zip \
    -Dfile=myproj.zip



curl -v \
    -F "r=releases" \
    -F "g=com.acme.widgets" \
    -F "a=widget" \
    -F "v=0.1-1" \
    -F "p=zip" \
    -F "file=@./readme.zip" \
    -u jenkins_user:Password001 \
    https://nexus.icare.nsw.gov.au/repository/bcpc-release-repo/

curl -X PUT -k -v -F r=releases -F hasPom=true -F file=@readme.zip -u jenkins_user:Password001 https://nexus.icare.nsw.gov.au/repository/bcpc-release-repo/com/example/artifact/1.0.0/readme-1.0.0.zip